# Scala scale library bulk conversion

All the tuning files for all synth formats that support FULL keyboard tunings for the entire Scala scale library. Current conversion is with version 89, December 2019 of the Scale Archive containing 4800 scale files.

D Burraston  - www.noyzelab.com - May 2020

**This repo contains =>**

**i] archive-all-full-mt-nyz.cmd** - command script if you want to run this in Scala yourself and generate all the tuning files for all the synth formats that support FULL keyboard tunings for the entire scale library.

**ii] scl-convs** - directory - the output of the command script is a bunch of tuning files for the synths described in the command file, as this is a LOT of files, this a zipfile for each synth type. Unzip a file to get the scale library for that synth. NOTE: the .tun files have had to be spread over 2 zips, as they are bigger than my 10MB upload limit as a single zipfile.. also the Waldorf formats for Wave and MicroWave throw up errors in Scala, so are currently incomplete.

**iii] empty-dirs.zip** - this is only for if you want to run the script and generate the files yourself in Scala. The *archive-all-full-mt-nyz.cmd* command file will expect to see these directories, so this will save you creating them.

For more information on the Scala microtuning program and the synths supported in these files =>
http://www.huygens-fokker.org/scala/

It should be obvious that multiple synths are sometimes supported within these formats, such as =>

DX7II files will load into TX802

TS10 files will load into TS12

TX81Z files will load into DX11 & V50

SY77 files will load into TG77/SY99/VL-1/VL-7

etc..

**how to keep me going ==>**
if u find this repo useful please think about supporting my work either thru my bandcamp page:
https://noyzelab.bandcamp.com/
or get in touch via noyzelab [at] gmail [dot] com
thanks, dave