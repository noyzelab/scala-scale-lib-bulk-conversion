! archive-all-nyz.cmd
! D Burraston  - www.noyzelab.com - May 2020
! Based on the archive command file supplied with scala to dump to MTS
! Converts the scl directory into all formats that support FULL keyboard tunings
! NOTE you will likely have to change the cd command to reflect where your scl directory is !!!
echo Transforming all scale files to all FULL keyboard tunings formats =>
echo
echo If needed, do SET MAP_FREQ first to change the frequency for 1/1.
set pause off
cd "C:\Program Files (x86)\Scala22\scl"
set synth 2
cd "C:\Program Files (x86)\Scala22\scl\dx7ii"
iterate "send/file %scl(-dx7ii.mid)" ..\*.scl
set synth 102
cd "C:\Program Files (x86)\Scala22\scl\tx81z"
iterate "send/file %scl(-tx81z.mid)" ..\*.scl
set synth 103
cd "C:\Program Files (x86)\Scala22\scl\proteus"
iterate "send/file %scl(-proteus.mid)" ..\*.scl
set synth 105
cd "C:\Program Files (x86)\Scala22\scl\asr10"
iterate "send/file %scl(-asr10.mid)" ..\*.scl
set synth 106
cd "C:\Program Files (x86)\Scala22\scl\morpheus"
iterate "send/file %scl(-morpheus.mid)" ..\*.scl
set synth 107
cd "C:\Program Files (x86)\Scala22\scl\107MTS"
iterate "send/file %scl(-107MTS.mid)" ..\*.scl
set synth 110
cd "C:\Program Files (x86)\Scala22\scl\oasys"
iterate "send/file %scl(-oasys.mid)" ..\*.scl
set synth 112
cd "C:\Program Files (x86)\Scala22\scl\tun"
iterate "send/file %scl(.tun)" ..\*.scl
set synth 113
cd "C:\Program Files (x86)\Scala22\scl\proteus2000"
iterate "send/file %scl(-proteus2000.mid)" ..\*.scl
set synth 114
cd "C:\Program Files (x86)\Scala22\scl\fluid"
iterate "send/file %scl(-fluid.cmd)" ..\*.scl
set synth 115
cd "C:\Program Files (x86)\Scala22\scl\maxmagic"
iterate "send/file %scl(-maxmagic.txt)" ..\*.scl
set synth 117
cd "C:\Program Files (x86)\Scala22\scl\timidity"
iterate "send/file %scl(-timidity.txt)" ..\*.scl
set synth 118
cd "C:\Program Files (x86)\Scala22\scl\csound"
iterate "send/file %scl(-csound.cps)" ..\*.scl
set synth 119
cd "C:\Program Files (x86)\Scala22\scl\bitheadz"
iterate "send/file %scl(-bitheadz.txt)" ..\*.scl
set synth 120
cd "C:\Program Files (x86)\Scala22\scl\reaktorsemi"
iterate "send/file %scl(-reaktorsemi.txt)" ..\*.scl
set synth 121
cd "C:\Program Files (x86)\Scala22\scl\reaktorfrq"
iterate "send/file %scl(-reaktorfrq.txt)" ..\*.scl
set synth 124
cd "C:\Program Files (x86)\Scala22\scl\reaktorntf"
iterate "send/file %scl(-reaktorntf.ntf)" ..\*.scl
set synth 126
cd "C:\Program Files (x86)\Scala22\scl\absynth"
iterate "send/file %scl(-absynth.gly)" ..\*.scl
set synth 129
cd "C:\Program Files (x86)\Scala22\scl\kontakt2"
iterate "send/file %scl(-kontakt2.ksp)" ..\*.scl
set synth 130
cd "C:\Program Files (x86)\Scala22\scl\chuck"
iterate "send/file %scl(-chuck.ck)" ..\*.scl
set synth 131
cd "C:\Program Files (x86)\Scala22\scl\msr2"
iterate "send/file %scl(-msr2.mid)" ..\*.scl
set synth 132
cd "C:\Program Files (x86)\Scala22\scl\sy77"
iterate "send/file %scl(-sy77.mid)" ..\*.scl
set synth 134
cd "C:\Program Files (x86)\Scala22\scl\ts10"
iterate "send/file %scl(-ts10.mid)" ..\*.scl
set synth 135
cd "C:\Program Files (x86)\Scala22\scl\maxcoll"
iterate "send/file %scl(-maxcoll.txt)" ..\*.scl
set synth 137
cd "C:\Program Files (x86)\Scala22\scl\obmx"
iterate "send/file %scl(-obmx.mid)" ..\*.scl
set synth 138
cd "C:\Program Files (x86)\Scala22\scl\haupt"
iterate "send/file %scl(-haupt.xml)" ..\*.scl
set synth 139
cd "C:\Program Files (x86)\Scala22\scl\datuner"
iterate "send/file %scl(-datuner.xml)" ..\*.scl
!
! NOTE seems to be a bug in my Scala version 2.42v with wave export 
! when it gets to => 11-31.scl Scala throws up "Computation error"
! 
!set synth 142
!cd "C:\Program Files (x86)\Scala22\scl\wave"
!iterate "send/file %scl(-wave.mid)" ..\*.scl
!
!
!
! NOTE seems to be a bug in my Scala version 2.42v with uwave export 
! when it gets to => 05-19.scl Scala throws up "File could not be created"
! 
!set synth 143
!cd "C:\Program Files (x86)\Scala22\scl\uwave"
!iterate "send/file %scl(-uwave.mid)" ..\*.scl
!
set synth 144
cd "C:\Program Files (x86)\Scala22\scl\octarone"
iterate "send/file %scl(-octarone.fxb)" ..\*.scl
